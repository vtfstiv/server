package ro.alexi.awbd.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ro.alexi.awbd.dtos.WarehouseDTO;
import ro.alexi.awbd.model.Warehouse;

@Mapper(componentModel = "spring", uses = {})
public interface WarehouseMapper extends EntityMapper<WarehouseDTO, Warehouse> {

    @Mapping(target = "id", source = "id")
    Warehouse toEntity(WarehouseDTO warehouseDTO);

    @Mapping(target = "id", source = "id")
    WarehouseDTO toDto(Warehouse warehouse);
}
