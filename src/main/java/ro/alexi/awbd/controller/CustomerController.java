package ro.alexi.awbd.controller;


import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.alexi.awbd.dtos.CustomerDTO;
import ro.alexi.awbd.service.CustomerService;

@RestController
@RequestMapping("/awbd/customer")
public class CustomerController {

    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @PostMapping("/register")
    public ResponseEntity<CustomerDTO> registerNewCustomer(@RequestBody CustomerDTO customerDTO) {
        CustomerDTO registeredCustomer = customerService.registerCustomer(customerDTO);

        return new ResponseEntity<>(registeredCustomer, HttpStatus.CREATED);
    }

    @PostMapping("/login")
    public ResponseEntity<CustomerDTO> loginAsCustomer(@RequestBody CustomerDTO customerDTO) throws Exception {
        CustomerDTO loggedCustomer = customerService.loginAsCustomer(customerDTO);

        return new ResponseEntity<>(loggedCustomer, HttpStatus.ACCEPTED);
    }

    @GetMapping("")
    public ResponseEntity<CustomerDTO> getLoggedCustomer() {
        CustomerDTO loggedCustomer = customerService.getLoggedCustomer();

        return new ResponseEntity<>(loggedCustomer, HttpStatus.ACCEPTED);
    }

}
