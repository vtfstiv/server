package ro.alexi.awbd.controller;


import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.alexi.awbd.dtos.ProductDTO;
import ro.alexi.awbd.dtos.SupplierDTO;
import ro.alexi.awbd.dtos.paginationDTO.SupplierPaginationResponse;
import ro.alexi.awbd.dtos.WarehouseDTO;
import ro.alexi.awbd.service.SupplierService;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/awbd/supplier")
public class SupplierController {

    private final SupplierService supplierService;

    public SupplierController(SupplierService supplierService) {
        this.supplierService = supplierService;
    }

    @GetMapping("/list")
    public ResponseEntity<List<SupplierDTO>> getAllWarehouses(@RequestParam int pageNumber, @RequestParam int pageSize, @RequestParam boolean completeList) {
        Pageable pageable = PageRequest.of(pageNumber, pageSize);

        SupplierPaginationResponse suppliersResponse = supplierService.getSuppliers(pageable, completeList);

        var headers = new HttpHeaders();
        headers.add("X-Total-Count", String.valueOf(suppliersResponse.getTotalItems()));

        return new ResponseEntity<>(suppliersResponse.getSuppliers(), headers, HttpStatus.OK);

    }

    @PostMapping("")
    public ResponseEntity<SupplierDTO> saveWarehouse(@RequestBody SupplierDTO supplierDTO) {
        SupplierDTO savedWarehouse = supplierService.saveSupplier(supplierDTO);

        return new ResponseEntity<>(savedWarehouse, HttpStatus.OK);
    }

    @DeleteMapping("")
    public void deleteWarehouse(@RequestParam UUID supplierId) {
        supplierService.deleteSupplier(supplierId);
    }

    @GetMapping("/{parentId}/warehouses")
    public ResponseEntity<List<WarehouseDTO>> getListOfWarehousesByParent(@PathVariable("parentId") UUID parentId) {
        List<WarehouseDTO> allWarehousesBySupplierId = supplierService.findAllWarehousesBySupplierId(parentId);

        return new ResponseEntity<>(allWarehousesBySupplierId, HttpStatus.OK);
    }

    @GetMapping("/{parentId}/products")
    public ResponseEntity<List<ProductDTO>> getListOfProductsByParent(@PathVariable("parentId") UUID parentId) {
        List<ProductDTO> allProductsBySupplierId = supplierService.findAllProductsBySupplierId(parentId);

        return new ResponseEntity<>(allProductsBySupplierId, HttpStatus.OK);
    }
}
