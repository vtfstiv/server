package ro.alexi.awbd.errors;

import org.springframework.http.HttpStatus;

public class GenericExceptionObject {
    private Integer status;
    private HttpStatus httpStatus;
    private String message;

    public GenericExceptionObject(Integer status, HttpStatus httpStatus, String message) {
        this.status = status;
        this.httpStatus = httpStatus;
        this.message = message;
    }

    public GenericExceptionObject() {
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
