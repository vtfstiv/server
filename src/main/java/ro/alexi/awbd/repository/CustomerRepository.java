package ro.alexi.awbd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ro.alexi.awbd.model.Customer;

import java.util.Optional;
import java.util.UUID;

public interface CustomerRepository extends JpaRepository<Customer, UUID> {

    Optional<Customer> findByEmail(String email);

    Optional<Customer> findByAuthority(String auth);
}
