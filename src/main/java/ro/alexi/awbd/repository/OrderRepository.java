package ro.alexi.awbd.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import ro.alexi.awbd.model.Order;

import java.util.UUID;

@Repository
public interface OrderRepository extends JpaRepository<Order, UUID> {

    @Query(value = "SELECT S_ORDER_NUMBER.nextval from dual", nativeQuery =
            true)
    Integer getNextOrderNumber();
}
