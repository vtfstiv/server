package ro.alexi.awbd.dtos;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;
import ro.alexi.awbd.model.Customer;

import java.util.Collection;
import java.util.UUID;

public class CustomUserDetails extends User {
    private UUID id;
    private Customer currentCustomer;

    public CustomUserDetails(String username, String password, Collection<? extends GrantedAuthority> authorities, UUID id, Customer currentCustomer) {
        super(username, password, authorities);
        this.id = id;
        this.currentCustomer = currentCustomer;

    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public Customer getCurrentCustomer() {
        return currentCustomer;
    }

    public void setCurrentCustomer(Customer currentCustomer) {
        this.currentCustomer = currentCustomer;
    }
}
