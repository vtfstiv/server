package ro.alexi.awbd.dtos;


import java.io.Serializable;
import java.util.UUID;


public class ProductDTO implements Serializable {

    private UUID id;
    private String productName;
    private Float price;
    private String description;
    private String image;
    private SupplierDTO productSupplier;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public SupplierDTO getProductSupplier() {
        return productSupplier;
    }

    public void setProductSupplier(SupplierDTO productSupplier) {
        this.productSupplier = productSupplier;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
