package ro.alexi.awbd.dtos.paginationDTO;

import ro.alexi.awbd.dtos.AddressDTO;

import java.util.List;

public class AddressPaginationResponse {

    private List<AddressDTO> addresses;
    private long totalItems;

    public long getTotalItems() {
        return totalItems;
    }

    public void setTotalItems(long totalItems) {
        this.totalItems = totalItems;
    }

    public List<AddressDTO> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<AddressDTO> addresses) {
        this.addresses = addresses;
    }
}
