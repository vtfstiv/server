package ro.alexi.awbd.service;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import ro.alexi.awbd.config.ConfigClass;
import ro.alexi.awbd.dtos.CustomUserDetails;
import ro.alexi.awbd.dtos.CustomerDTO;
import ro.alexi.awbd.errors.CustomException;
import ro.alexi.awbd.jwt.JwtTokenUtil;
import ro.alexi.awbd.mapper.CustomerMapper;
import ro.alexi.awbd.model.Customer;
import ro.alexi.awbd.repository.CustomerRepository;
import ro.alexi.awbd.security.SecurityUserService;
import ro.alexi.awbd.security.SecurityUtils;

import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

@Service
public class CustomerService {

    private final CustomerRepository customerRepository;
    private final CustomerMapper customerMapper;
    private final AuthenticationManager authenticationManager;
    private final JwtTokenUtil jwtTokenUtil;
    private final SecurityUserService securityUserService;
    private final ConfigClass configClass;


    public CustomerService(CustomerRepository customerRepository, CustomerMapper customerMapper, AuthenticationManager authenticationManager, JwtTokenUtil jwtTokenUtil, SecurityUserService securityUserService, ConfigClass configClass) {
        this.customerRepository = customerRepository;
        this.customerMapper = customerMapper;
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
        this.securityUserService = securityUserService;
        this.configClass = configClass;
    }

    public CustomerDTO loginAsCustomer(CustomerDTO customerDTO) throws Exception {
        Optional<Customer> customerByEmail = customerRepository.findByEmail(customerDTO.getEmail());

        if (customerByEmail.isEmpty()) {
            throw new CustomException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, "Clientul nu exista!");
        } else {
            authenticate(customerDTO.getEmail(), customerDTO.getPassword());
            UserDetails userDetails = securityUserService.loadUserByUsername(customerDTO.getEmail());
            String token = jwtTokenUtil.generateToken(userDetails);

            SecurityUtils.setJwtToken(token);

            CustomerDTO existingCustomer = customerMapper.toDto(customerByEmail.get());
            existingCustomer.setToken(token);

            authenticate(customerDTO.getEmail(), customerDTO.getPassword());

            return existingCustomer;
        }
    }

    private void authenticate(String email, String password) throws Exception {
        Objects.requireNonNull(email);
        Objects.requireNonNull(password);
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new CustomException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, "Username sau parola gresite");
        }
    }

    public CustomerDTO registerCustomer(CustomerDTO customerDTO) {

        Optional<Customer> byEmail = customerRepository.findByEmail(customerDTO.getEmail());
        if (byEmail.isEmpty()) {
            customerDTO.setPassword(configClass.passwordEncoder().encode(customerDTO.getPassword()));
            customerDTO.setAuthority("CUSTOMER");
            return customerMapper.toDto(customerRepository.save(customerMapper.toEntity(customerDTO)));
        } else {
            throw new CustomException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, "Clientul exista deja!");
        }
    }

    public CustomerDTO getLoggedCustomer() {
        UUID userId = SecurityUtils.getUserId();

        Optional<Customer> byId = customerRepository.findById(userId);

        if (byId.isEmpty()) {
            return null;
        } else {
            String jwtToken = SecurityUtils.getJwtToken();
            CustomerDTO customerDTO = customerMapper.toDto(byId.get());
            customerDTO.setToken(jwtToken);
            return customerDTO;
        }
    }
}
