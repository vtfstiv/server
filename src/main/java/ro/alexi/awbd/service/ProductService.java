package ro.alexi.awbd.service;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import ro.alexi.awbd.dtos.ProductDTO;
import ro.alexi.awbd.dtos.paginationDTO.ProductPaginationResponse;
import ro.alexi.awbd.errors.CustomException;
import ro.alexi.awbd.mapper.ProductMapper;
import ro.alexi.awbd.model.Product;
import ro.alexi.awbd.model.Supplier;
import ro.alexi.awbd.repository.ProductRepository;
import ro.alexi.awbd.security.SecurityUtils;

import java.util.Optional;
import java.util.UUID;

@Service
public class ProductService {
    private final ProductRepository productRepository;
    private final ProductMapper productMapper;

    public ProductService(ProductRepository productRepository, ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
    }

    public ProductPaginationResponse getProducts(Pageable pageable) {
        ProductPaginationResponse productPaginationResponse = new ProductPaginationResponse();

        Page<Product> products = productRepository.findAll(pageable);

        productPaginationResponse.setTotalItems(products.getTotalElements());
        productPaginationResponse.setProducts(productMapper.toDto(products.getContent()));

        return productPaginationResponse;
    }

    public ProductDTO saveProduct(ProductDTO product) {
        Optional<Product> optionalProduct = productRepository.findFirstByProductNameAndPriceAndProductSupplierId(product.getProductName(), product.getPrice(), product.getProductSupplier().getId());

        if (optionalProduct.isEmpty() || product.getId() != null) {
            Product savedProduct = productRepository.save(productMapper.toEntity(product));

            return productMapper.toDto(savedProduct);
        } else {
            throw new CustomException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, "Produsul exista deja");
        }
    }

    public void deleteProduct(UUID productId) {
        Optional<Product> byId = productRepository.findById(productId);

        if (byId.isPresent()) {
            productRepository.delete(byId.get());
        } else {
            throw new CustomException(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, "Produsul nu a fost gasit");
        }
    }
}
