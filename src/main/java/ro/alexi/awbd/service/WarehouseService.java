package ro.alexi.awbd.service;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import ro.alexi.awbd.dtos.WarehouseDTO;
import ro.alexi.awbd.dtos.paginationDTO.WarehousePaginationResponse;
import ro.alexi.awbd.errors.CustomException;
import ro.alexi.awbd.mapper.WarehouseMapper;
import ro.alexi.awbd.model.Warehouse;
import ro.alexi.awbd.repository.WarehouseRepository;

import java.util.Optional;
import java.util.UUID;

@Service
public class WarehouseService {

    private final WarehouseRepository warehouseRepository;
    private final WarehouseMapper warehouseMapper;

    public WarehouseService(WarehouseRepository warehouseRepository, WarehouseMapper warehouseMapper) {
        this.warehouseRepository = warehouseRepository;
        this.warehouseMapper = warehouseMapper;
    }

    public WarehousePaginationResponse getWarehouses(Pageable pageable) {
        Page<Warehouse> warehousePage = warehouseRepository.findAll(pageable);

        WarehousePaginationResponse warehousePaginationResponse = new WarehousePaginationResponse();

        warehousePaginationResponse.setTotalItems(warehousePage.getTotalElements());
        warehousePaginationResponse.setWarehouses(warehouseMapper.toDto(warehousePage.getContent()));

        return warehousePaginationResponse;
    }

    public WarehouseDTO saveWarehouse(WarehouseDTO warehouseDTO) {
        Optional<Warehouse> warehouseByAddress = warehouseRepository.findByAddress(warehouseDTO.getAddress());
        if (warehouseByAddress.isEmpty() || warehouseDTO.getId() != null) {
            Warehouse savedWarehouse = warehouseRepository.save(warehouseMapper.toEntity(warehouseDTO));
            return warehouseMapper.toDto(savedWarehouse);
        }
        throw new CustomException(HttpStatus.BAD_REQUEST.value(), HttpStatus.BAD_REQUEST, "Exista deja un depozit la acea adresa");
    }

    public void deleteWarehouse(UUID warehouseId) {
        Optional<Warehouse> warehouseById = warehouseRepository.findById(warehouseId);

        if (warehouseById.isEmpty()) {
            throw new CustomException(HttpStatus.NOT_FOUND.value(), HttpStatus.NOT_FOUND, "Depozitul nu a fost gasit");
        }

        warehouseRepository.delete(warehouseById.get());
    }
}
