package ro.alexi.awbd.service;


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ro.alexi.awbd.dtos.OrderDTO;
import ro.alexi.awbd.dtos.paginationDTO.OrderPaginationResponse;
import ro.alexi.awbd.mapper.OrderMapper;
import ro.alexi.awbd.model.Order;
import ro.alexi.awbd.repository.OrderRepository;
import ro.alexi.awbd.security.SecurityUtils;

import javax.transaction.Transactional;
import java.time.ZonedDateTime;

@Service
public class OrderService {
    private final OrderRepository orderRepository;
    private final OrderMapper orderMapper;

    public OrderService(OrderRepository orderRepository, OrderMapper orderMapper) {
        this.orderRepository = orderRepository;
        this.orderMapper = orderMapper;
    }

    @Transactional
    public OrderDTO placeOrder(OrderDTO order) {
        order.setOrderDate(ZonedDateTime.now());
        Integer nextOrderNumber = orderRepository.getNextOrderNumber();
        order.setOrderNumber((long) (nextOrderNumber));

        Order placedOrder = orderRepository.save(orderMapper.toEntity(order));

        placedOrder.setCustomer(SecurityUtils.getCurrentUser());
        orderRepository.save(placedOrder);

        return orderMapper.toDto(placedOrder);

    }

    public OrderPaginationResponse getOrders(Pageable pageable) {
        OrderPaginationResponse orderPaginationResponse = new OrderPaginationResponse();

        Page<Order> orders = orderRepository.findAll(pageable);

        orderPaginationResponse.setTotalItems(orders.getTotalElements());
        orderPaginationResponse.setOrders(orderMapper.toDto(orders.getContent()));

        return orderPaginationResponse;
    }
}
